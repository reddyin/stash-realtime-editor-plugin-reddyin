package com.atlassian.stash.plugin.editor.io;

import com.atlassian.stash.scm.CommandInputHandler;
import com.atlassian.utils.process.StringInputHandler;

import static com.google.common.base.Preconditions.checkNotNull;

public class StringCommandInputHandler extends StringInputHandler implements CommandInputHandler {

    public StringCommandInputHandler(String content) {
        super(checkNotNull(content, "content"));
    }

}
