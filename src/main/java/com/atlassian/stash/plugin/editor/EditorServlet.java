package com.atlassian.stash.plugin.editor;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.security.random.DefaultSecureTokenGenerator;
import com.atlassian.security.random.SecureTokenGenerator;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.ContentService;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.io.TypeAwareOutputSupplier;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.plugin.editor.io.IOHandlers;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryMetadataService;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.scm.git.GitScmCommandBuilder;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import static com.atlassian.stash.plugin.editor.io.IOHandlers.stringIn;
import static com.atlassian.stash.plugin.editor.io.IOHandlers.stringOut;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.apache.commons.lang.StringUtils.isBlank;

public class EditorServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(EditorServlet.class);

    private static final long MAX_FILE_SIZE = 1024 * 1024; // 1 mb
    private static final long LINE_SEP_SAMPLE_SIZE = 10 * 1024; // 10kb

    private final RepositoryService repositoryService;
    private final StashAuthenticationContext authenticationContext;
    private final I18nService i18nService;
    private final WebResourceManager webResourceManager;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final ContentService contentService;
    private final GitScm gitScm;
    private final NavBuilder navBuilder;
    private final RepositoryMetadataService repositoryMetadataService;
    private final CommitService commitService;
    private final PermissionService permissionService;
    private final LoginUriProvider loginUriProvider;
    private final RequestFactory requestFactory;
    private final CollaborationService collaborationService;
    private ServletSupport servletSupport;
    private final SecureTokenGenerator secureTokenGenerator;

    private final Map<String, String> ext2mime = Maps.newConcurrentMap();

    public EditorServlet(RepositoryService repositoryService, StashAuthenticationContext authenticationContext,
                         I18nService i18nService, WebResourceManager webResourceManager,
                         SoyTemplateRenderer soyTemplateRenderer, ContentService contentService,
                         GitScm gitScm, NavBuilder navBuilder, RepositoryMetadataService repositoryMetadataService,
                         CommitService commitService, PermissionService permissionService,
                         LoginUriProvider loginUriProvider, RequestFactory requestFactory,
                         CollaborationService collaborationService, ServletSupport servletSupport) {
        this.repositoryService = repositoryService;
        this.authenticationContext = authenticationContext;
        this.i18nService = i18nService;
        this.webResourceManager = webResourceManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.contentService = contentService;
        this.gitScm = gitScm;
        this.navBuilder = navBuilder;
        this.repositoryMetadataService = repositoryMetadataService;
        this.commitService = commitService;
        this.permissionService = permissionService;
        this.loginUriProvider = loginUriProvider;
        this.requestFactory = requestFactory;
        this.collaborationService = collaborationService;
        this.servletSupport = servletSupport;
        this.secureTokenGenerator = DefaultSecureTokenGenerator.getInstance();
        initMimeTypes();
    }

    private void initMimeTypes() {
        ResourceBundle mimeTypes = ResourceBundle.getBundle("mime");
        for (String key : mimeTypes.keySet()) {
            for (String extension : mimeTypes.getString(key).split(" ")) {
                if (!isBlank(extension)) {
                    ext2mime.put(extension, key);
                }
            }
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final PathAtCommit pathAtCommit = resolvePathAtCommit(req, resp);
        if (pathAtCommit == null) {
            return;
        }

        boolean isCollaborationEnabled = collaborationService.isEnabledForRepository(pathAtCommit.getRepository());

        String id = req.getParameter("id");
        if (isBlank(id) && isCollaborationEnabled) {
            // generate secure id to identify session - only users with the URL will have access to the edit session.
            id = secureTokenGenerator.generateToken();
            StringBuffer urlWithId = req.getRequestURL();
            if (isBlank(req.getQueryString())) {
                urlWithId.append("?id=").append(id);
            } else {
                urlWithId
                    .append("?").append(req.getQueryString())
                    .append("&id=").append(id);
            }
            resp.sendRedirect(urlWithId.toString());
            return;
        }

        final Map<String, Object> context = createContext(pathAtCommit);

        String sizeStr = gitScm.getCommandBuilderFactory()
                .builder(pathAtCommit.getRepository())
                .command("cat-file")
                .argument("-s")
                .argument(pathAtCommit.toObjectName())
                .build(IOHandlers.stringOut())
                .call();

        long size = Long.parseLong(checkNotNull(sizeStr, "sizeStr").trim());
        if (size > MAX_FILE_SIZE) {
            String errorMessage = i18nService.getText("stash.repo.servlet.file.size.too.big",
                    "{0} is {1} bytes (@ {2}). The maximum editable file size is {3} bytes, sorry!",
                    pathAtCommit.getPath(), size, pathAtCommit.getCommit(), MAX_FILE_SIZE);
            renderErrorMessage(resp, context, errorMessage);
            return;
        }

        // resolve content
        final ByteArrayOutputStream contentBuffer = new ByteArrayOutputStream();
        contentService.streamFile(pathAtCommit.getRepository(), pathAtCommit.getCommit(), pathAtCommit.getPath(), new TypeAwareOutputSupplier() {
            @Override
            public OutputStream getStream(@Nonnull String contentType) throws IOException {
                if ("text/plain".equals(contentType)) {
                    String path = pathAtCommit.getPath();
                    int extIndex = path.lastIndexOf(".");
                    if (extIndex > -1 && extIndex < path.length() - 1) {
                        String ext = path.substring(extIndex + 1);
                        // prefer case-sensitive first
                        String mimeByExt = ext2mime.get(ext);
                        if (mimeByExt == null) {
                            // try lowercase
                            mimeByExt = ext2mime.get(ext.toLowerCase());
                        }
                        if (mimeByExt != null) {
                            contentType = mimeByExt;
                        }
                    }
                }
                context.put("contentType", contentType);
                return contentBuffer;
            }
        });
        String content = contentBuffer.toString();

        context.put("lineSeparator", LineSeparator.resolve(content, LINE_SEP_SAMPLE_SIZE).name());
        context.put("content", content);
        context.put("fileMode", resolveFileMode(pathAtCommit));

        if (isCollaborationEnabled) {
            webResourceManager.requireResourcesForContext("plugin.page.editor.collaboration");
            context.put("sessionId", id);
        }

        context.put("canPublish", permissionService.hasRepositoryPermission(pathAtCommit.getRepository(),
                Permission.REPO_WRITE));

        render(resp, "stash.editor.page", context);
    }

    private String resolveFileMode(PathAtCommit pathAtCommit) {
        String treeish = pathAtCommit.getCommit() + ":" + pathAtCommit.getParentDirectory();
        String fileMode = gitScm.getCommandBuilderFactory()
                .builder(pathAtCommit.getRepository())
                .command("ls-tree")
                .argument(treeish)
                .build(IOHandlers.fileMode(pathAtCommit.getFilename()))
                .call();
        return checkNotNull(fileMode, "fileMode");
    }

    private Map<String, Object> createContext(PathAtCommit pathAtCommit) {
        final Map<String, Object> context = Maps.newHashMap();
        context.put("repository", pathAtCommit.getRepository());
        context.put("path", pathAtCommit.getPath());
        context.put("at", pathAtCommit.getCommit());
        return context;
    }

    private void renderErrorMessageNoContext(HttpServletResponse resp, String errorMessage) throws IOException, ServletException {
        render(resp, "stash.editor.notAvailable", ImmutableMap.<String, Object>of("errorMessage", errorMessage));
    }

    private void renderErrorMessage(HttpServletResponse resp, Map<String, Object> context, String errorMessage) throws IOException, ServletException {
        context.put("errorMessage", errorMessage);
        render(resp, "stash.editor.error", context);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PathAtCommit pathAtCommit = resolvePathAtCommit(req, resp);
        if (pathAtCommit == null) {
            return;
        }

        if (!permissionService.hasRepositoryPermission(pathAtCommit.getRepository(), Permission.REPO_WRITE)) {
            renderErrorMessage(resp, createContext(pathAtCommit),
                    i18nService.getText("stash.repo.servlet.no.write.permission",
                    "You have insufficient permissions to write to this repository."));
            return;
        }

        String message = req.getParameter("message");
        if (isBlank(message)) {
            message = "Online edit.";
        }

        String fileMode = req.getParameter("fileMode");
        if (isBlank(fileMode)) {
            fileMode = "100644"; // what-evs
        }

        String lineSeparatorId = req.getParameter("lineSeparator");
        LineSeparator lineSeparator;
        if (isBlank(lineSeparatorId)) {
            lineSeparator = LineSeparator.LF;
        } else {
            lineSeparator = LineSeparator.valueOf(lineSeparatorId);
        }

        String content = req.getParameter("content");
        content = content.replaceAll("\\r\\n", lineSeparator.getSeparator());

        // bash equivalent:

        // PARENT_COMMIT=cafebabe
        // GIT_INDEX_FILE=$(mktemp /tmp/temp.XXXXXX)
        // echo "using $GIT_INDEX_FILE as index file"
        // OBJ_SHA=$(echo "my little pony" | git hash-object -w --stdin)
        // echo "object SHA: $OBJ_SHA"
        // git read-tree $PARENT_COMMIT
        // git update-index --add --cacheinfo 100644 $OBJ_SHA pom.xml
        // NEW_TREE=$(git write-tree)
        // NEW_COMMIT=$(git commit-tree -p $PARENT_COMMIT -m "my little commit" $NEW_TREE)
        // DATE=$(date +%s)
        // git branch branch-$DATE $NEW_COMMIT

        StashUser currentUser = authenticationContext.getCurrentUser();
        String displayName, username, email;
        if (currentUser != null) {
            displayName = currentUser.getDisplayName();
            username = currentUser.getName().replaceAll("[^a-zA-Z0-9\\-]", "");
            email = currentUser.getEmailAddress();
        } else {
            displayName = "Anonymous";
            username = "anon";
            email = "anon@example.com";
        }

        TemporaryIndexCommandBuilderFactory builderFactory = new TemporaryIndexCommandBuilderFactory(
                gitScm.getCommandBuilderFactory(), pathAtCommit.getRepository(), displayName, email);

        String objSha = checkNotNull(builderFactory.builder()
                .command("hash-object")
                .argument("-w")
                .argument("--stdin")
                .inputHandler(stringIn(content))
                .build(stringOut())
                .call(), "objSha");

        builderFactory.builder()
                .command("read-tree")
                .argument(pathAtCommit.getCommit())
                .build(stringOut())
                .call();

        builderFactory.builder()
                .command("update-index")
                .argument("--add")
                .argument("--cacheinfo")
                .argument(fileMode)
                .argument(objSha)
                .argument(pathAtCommit.getPath())
                .build(stringOut())
                .call();

        String newTree = checkNotNull(builderFactory.builder()
                .command("write-tree")
                .build(stringOut())
                .call(), "newTree");

        String newCommit = checkNotNull(builderFactory.builder()
                .command("commit-tree")
                .argument(newTree)
                .argument("-p")
                .argument(pathAtCommit.getCommit())
                .inputHandler(stringIn(message))
                .build(stringOut())
                .call(), "newCommit");

        // generate branch name
        String varier = StringUtils.reverse(Long.toHexString(System.currentTimeMillis()));
        String newBranch = String.format("%s-edit-%s", username, varier);

        builderFactory.builder()
                .command("branch")
                .argument(newBranch)
                .argument(newCommit)
                .build(stringOut())
                .call();

        // redirect current user to create pull request
        String createPullRequestHref = navBuilder
                .repo(pathAtCommit.getRepository())
                .createPullRequest()
                .sourceBranch(newBranch)
                .buildAbsolute();

        // create link to new commit for other clients
        String newCommitHref = navBuilder
                .repo(pathAtCommit.getRepository())
                .changeset(newCommit.trim())
                .buildAbsolute();

        String id = req.getParameter("id");
        // don't require a session, this end-point can be used beyond the scope of a firebase edit session
        if (!isBlank(id)) {
            // notify firebase that changes have been published as a new branch
            String payLoad = new Gson().toJson(ImmutableMap.of("user", displayName, "branchLink", newCommitHref));
            try {
                requestFactory
                        .createRequest(Request.MethodType.PUT, "https://stasheditor.firebaseio.com/" + id + "/published.json")
                        .setRequestBody(payLoad)
                        .execute();
            } catch (ResponseException e) {
                log.error("Failed to notify firebase of published branch.", e);
            }
        }

        resp.sendRedirect(createPullRequestHref);
    }

    private PathAtCommit resolvePathAtCommit(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Either<String, EntityDescriptor> entityDescriptorOrError = servletSupport.resolveEntity(req, resp);

        if (entityDescriptorOrError.isLeft()) {
            String error = entityDescriptorOrError.left().get();
            if (!isBlank(error)) {
                renderErrorMessageNoContext(resp, error);
            }
            return null;
        }

        EntityDescriptor entityDescriptor = entityDescriptorOrError.right().get();

        if (isBlank(entityDescriptor.getPath())) {
            resp.sendError(SC_BAD_REQUEST, "A repository path is required.");
            return null;
        }

        return new PathAtCommit(entityDescriptor.getRepository(), entityDescriptor.getPath(), entityDescriptor.getAt());
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            webResourceManager.requireResourcesForContext("plugin.page.editor");
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.atlassian.stash.plugin.stash-editor-plugin:editor-server-side-soy", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    private static class TemporaryIndexCommandBuilderFactory {

        private final String indexPath;
        private final GitCommandBuilderFactory factory;
        private final Repository repository;
        private final String authorEmail;
        private final String authorName;

        private TemporaryIndexCommandBuilderFactory(GitCommandBuilderFactory factory, Repository repository,
                                                    String authorName, String authorEmail) throws IOException {
            this.factory = factory;
            this.repository = repository;
            this.authorName = authorName;
            this.authorEmail = authorEmail;

            File indexFile = File.createTempFile("git-index", null);
            if (!indexFile.delete()) { // this is a bit silly
                throw new IllegalStateException("Couldn't delete temporary file to be used as alt git-index");
            }
            this.indexPath = indexFile.getAbsolutePath();
        }

        public GitScmCommandBuilder builder() {
            return factory.builder(repository)
                    .environment("GIT_INDEX_FILE", indexPath)
                    .environment("GIT_AUTHOR_NAME", authorName)
                    .environment("GIT_AUTHOR_EMAIL", authorEmail)
                    .environment("GIT_COMMITTER_NAME", authorName)
                    .environment("GIT_COMMITTER_EMAIL", authorEmail);
        }

    }

}
