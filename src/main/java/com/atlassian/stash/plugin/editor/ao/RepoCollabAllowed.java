package com.atlassian.stash.plugin.editor.ao;
 
import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.RawEntity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;

@Preload
public interface RepoCollabAllowed extends RawEntity<Integer>
{
    @NotNull
    @PrimaryKey("REPOSITORY_ID")
    Integer getRepositoryId();
}